package com.kyc.junit.scanExtract;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import com.kyc.cucumber.serenity.scanExtractSteps;

import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import net.thucydides.junit.annotations.UseTestDataFrom;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom("testdata/kyc/publicRegistry/validate.csv")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class validate {
	
	private String document_type;
	private String consent;
	private String consent_purpose;
	private String kyc_validate;
	
	@Steps
	scanExtractSteps steps;
	
	@Title("This test makes Scan Extract validate call")
	@Test
	public void test01() {
		
		
	}
	
}
