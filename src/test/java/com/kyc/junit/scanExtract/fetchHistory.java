package com.kyc.junit.scanExtract;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.kyc.cucumber.serenity.scanExtractSteps;
import com.testbase.scanExtract;
import com.utils.databaseValidate;
import io.restassured.response.ValidatableResponse;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import net.thucydides.junit.annotations.UseTestDataFrom;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom("testdata/kyc/scanExtract/fetchHistory.csv")
public class fetchHistory extends scanExtract {
	
	private String fromDate;
	private String toDate;
	private String kycStatus;
	private String ocrStatus;
	
	@Steps
	scanExtractSteps steps;
	
	@Title("OCR Fetch History")
	@Test
	public void test_01() {
		
		ValidatableResponse resp = steps.fetchHistory(fromDate, toDate, kycStatus, ocrStatus)
										.statusCode(200);
		String txnId = resp.extract().path("decentroTxnId");
		
		int dbRespCode = databaseValidate.getDctlRespCode(txnId);
		assertEquals(200, dbRespCode);
	}
}