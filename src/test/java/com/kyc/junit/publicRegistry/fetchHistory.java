package com.kyc.junit.publicRegistry;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.kyc.cucumber.serenity.publicRegistrySteps;
import com.testbase.publicRegistry;
import com.utils.databaseValidate;
import io.restassured.response.ValidatableResponse;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import net.thucydides.junit.annotations.UseTestDataFrom;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom("testdata/kyc/publicRegistry/fetchHistory.csv")
public class fetchHistory extends publicRegistry {
	
	private String fromDate;
	private String toDate;
	private String status;
	
	@Steps
	publicRegistrySteps steps;
	
	@Title("PR Fetch History")
	@Test
	public void test_01() {
		
		ValidatableResponse resp = steps.fetchHistory(fromDate, toDate, status)
										.statusCode(200);
		
		String txnId = resp.extract().path("decentroTxnId");
		
		int dbRespCode = databaseValidate.getDctlRespCode(txnId);
		assertEquals(200, dbRespCode);
	}
}