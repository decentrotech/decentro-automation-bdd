package com.kyc.junit.publicRegistry;

import static org.junit.Assert.assertEquals;

import java.util.Random;

import org.junit.Test;
import org.junit.runner.RunWith;
import com.kyc.cucumber.serenity.publicRegistrySteps;
import com.testbase.publicRegistry;
import com.utils.databaseValidate;
import com.utils.fetchFile;

import io.restassured.response.ValidatableResponse;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import net.thucydides.junit.annotations.UseTestDataFrom;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom("testdata/kyc/publicRegistry/aadhaar.csv")
public class validateAadhaar extends publicRegistry {
	
	private String documentType;
	
	@Steps
	publicRegistrySteps steps;
	
	@Title("This test makes validate call for Aadhaar XML")
	@Test
	public void test01() {
		
		ValidatableResponse resp = steps.validateAadhaarXML(documentType);
				
		int apiRespCode = resp.extract().statusCode();
		int dbRespCode = databaseValidate.getDctlRespCode(resp.extract().path("decentroTxnId"));
		assertEquals(apiRespCode, dbRespCode);	
	}
}