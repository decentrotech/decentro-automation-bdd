package com.kyc.junit.publicRegistry;

import static org.junit.Assert.assertEquals;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import com.kyc.cucumber.serenity.publicRegistrySteps;
import com.testbase.publicRegistry;
import com.utils.databaseValidate;
import io.restassured.response.ValidatableResponse;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;

@RunWith(SerenityParameterizedRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class va extends publicRegistry {
	
	private String consent;
	private String consent_purpose;
	
	@Steps
	publicRegistrySteps steps;
	
	@Title("This test makes Public Registry validate call for Aadhaar")
	@Test
	public void test01() {
		
		ValidatableResponse resp = steps.validateAadhaarXML("aadhaar");
		int apiRespCode = resp.extract().statusCode();
		int dbRespCode = databaseValidate.getDctlRespCode(resp.extract().path("decentroTxnId"));
		assertEquals(apiRespCode, dbRespCode);
	}
}