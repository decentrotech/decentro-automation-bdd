package com.kyc.junit.publicRegistry;

import static org.junit.Assert.assertEquals;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import com.kyc.cucumber.serenity.publicRegistrySteps;
import com.testbase.publicRegistry;
import com.utils.databaseValidate;
import io.restassured.response.ValidatableResponse;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import net.thucydides.junit.annotations.UseTestDataFrom;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom("testdata/kyc/publicRegistry/validate.csv")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class validate extends publicRegistry {
	
	private String document_type;
	private String id_number;
	private String consent;
	private String consent_purpose;
	
	@Steps
	publicRegistrySteps steps;
	
	@Title("This test makes Public Registry validate call")
	@Test
	public void test01() {
		
		ValidatableResponse resp = steps.validate(document_type, id_number, consent, consent_purpose);
		
		int apiRespCode = resp.extract().statusCode();
		int dbRespCode = databaseValidate.getDctlRespCode(resp.extract().path("decentroTxnId"));
		assertEquals(apiRespCode, dbRespCode);
	}
}