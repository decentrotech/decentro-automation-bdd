package com.kyc.cucumber.serenity;

import java.util.Random;

import org.json.simple.JSONObject;

import com.utils.getHeadersJson;
import com.utils.referenceIdGenerator;
import com.utils.fetchFile;

import io.restassured.response.ValidatableResponse;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class scanExtractSteps {
	
	@Step("Fetch History from {0} to {1} for kycStatus {2} and ocrStatus {3}")
	public ValidatableResponse fetchHistory(String fromDate, String toDate, String kycStatus, String ocrStatus) {
		return SerenityRest.rest().given().relaxedHTTPSValidation()
				.headers(getHeadersJson.getHeader("KYC Headers"))
				.queryParam("from", fromDate)
				.queryParam("to", toDate)
				.queryParam("kyc_status", kycStatus)
				.queryParam("ocr_status", ocrStatus)
			.when()
				.get("/history")
			.then();
	}
	
	@Step("Validate ")
	public ValidatableResponse validate() {
		
		String reference_id = referenceIdGenerator.getRandomString();
		int no = fetchFile.fetchNumberOfFolders("aadhaar");
		Random ran = new Random();
		int n = 1+ran.nextInt(no);
		String  file_content= fetchFile.getAadhaarXml(n,"xml");
		String file_password = fetchFile.getAadhaarXml(n,"pw");
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("reference_id", reference_id);
		jsonObj.put("document_type", "aadhaar");
		jsonObj.put("file_base_64_string", file_content);
		jsonObj.put("file_password", file_password);
		jsonObj.put("file_type", "xml");
		jsonObj.put("consent", "Y");
		jsonObj.put("consent_purpose", "For test scenarios purpose only");
		System.out.println("Request Payload:  "+jsonObj);
		String jsonBody = jsonObj.toJSONString();
		
		return SerenityRest.rest().given().relaxedHTTPSValidation()
					.contentType("application/json")
					.headers(getHeadersJson.getHeader("KYC Headers"))
				.when()
					.body(jsonBody)
					.post("/validate")
				.then();
	}
}