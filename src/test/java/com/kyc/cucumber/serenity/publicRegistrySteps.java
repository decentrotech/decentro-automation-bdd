package com.kyc.cucumber.serenity;

import java.util.Random;

import org.json.simple.JSONObject;

import com.utils.fetchFile;
import com.utils.getHeadersJson;
import com.utils.referenceIdGenerator;

import io.restassured.response.ValidatableResponse;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class publicRegistrySteps {
	
	@Step("Fetch History from {0} to {1} for status {2}")
	public ValidatableResponse fetchHistory(String fromDate, String toDate, String status) {
		return SerenityRest.rest().given().relaxedHTTPSValidation()
				.headers(getHeadersJson.getHeader("KYC Headers"))
				.queryParam("from", fromDate)
				.queryParam("to", toDate)
				.queryParam("kyc_status", status)
			.when()
				.get("/history")
			.then();
	}
	
	@Step("Validate for {0} with ID: {1}")
	public ValidatableResponse validate(String document_type, String id_number, String consent, String consent_purpose) {
		
		String reference_id = referenceIdGenerator.getRandomString();
		JSONObject body = new JSONObject();
		body.put("reference_id", reference_id);
		body.put("document_type", document_type);
		body.put("id_number", id_number);
		body.put("consent", consent);
		body.put("consent_purpose", consent_purpose);
		return SerenityRest.rest().given().relaxedHTTPSValidation()
					.contentType("application/json")
					.headers(getHeadersJson.getHeader("KYC Headers"))
				.when()
					.body(body)
					.post("/validate")
				.then();
				
	}
	
	@Step("Validate for Aadhaar with XML file")
	public ValidatableResponse validateAadhaarXML(String document_type) {
		
		String reference_id = referenceIdGenerator.getRandomString();
		Random ran = new Random();
		int n = 1 + ran.nextInt(fetchFile.fetchNumberOfFolders(document_type));
		String  file_content= fetchFile.getAadhaarXml(n,"xml");
		String file_password = fetchFile.getAadhaarXml(n,"pw");
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("reference_id", reference_id);
		jsonObj.put("document_type", "aadhaar");
		jsonObj.put("file_base_64_string", file_content);
		jsonObj.put("file_password", file_password);
		jsonObj.put("file_type", "xml");
		jsonObj.put("consent", "Y");
		jsonObj.put("consent_purpose", "For test scenarios purpose only");
		String jsonBody = jsonObj.toJSONString();
		System.out.println("Request Payload:  "+jsonBody);
		
		return SerenityRest.rest().given().relaxedHTTPSValidation()
					.log().all()
					.contentType("application/json")
					.headers(getHeadersJson.getHeader("KYC Headers"))
				.when()
					.body(jsonBody)
					.post("/validate")
				.then()
					.log().all();
	}
}