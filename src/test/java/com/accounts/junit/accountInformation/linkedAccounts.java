package com.accounts.junit.accountInformation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.accounts.cucumber.serenity.accountInformationSteps;
import com.schemas.schemaUtils;
import com.testbase.accountInformation;
import com.utils.databaseValidate;

import io.restassured.response.ValidatableResponse;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import net.thucydides.junit.annotations.UseTestDataFrom;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom("testdata/accounts/accountInformation/linkedaccounts.csv")
public class linkedAccounts extends accountInformation{
	
	private String mobile;
	private String customer_id;
	
	@Steps
	accountInformationSteps steps;
	
	@Title("Fetch Linked Accounts")
	@Test
	public void test01() {
		ValidatableResponse resp = steps.getLinkedAccounts(mobile, customer_id);
		
		int apiRespCode = resp.extract().statusCode();
		int dbRespCode = databaseValidate.getDctlRespCode(resp.extract().path("decentroTxnId"));
		assertEquals(apiRespCode, dbRespCode);
		
		String respBody = resp.extract().body().asString();
		String schemaText = schemaUtils.getSchemaString("accounts", "linkedAccounts");
		assertTrue(schemaUtils.validate(schemaText, respBody));
	}
}