package com.accounts.junit.moneyTransfer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.accounts.cucumber.serenity.moneyTransferSteps;
import com.schemas.schemaUtils;
import com.testbase.moneyTransfer;
import com.utils.databaseValidate;
import com.utils.referenceIdGenerator;

import io.restassured.response.ValidatableResponse;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import net.thucydides.junit.annotations.UseTestDataFrom;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom("testdata/accounts/moneytransfer/validate.csv")
public class validateAccount extends moneyTransfer{
	
	static String reference_id = referenceIdGenerator.getRandomString();
	
	private String transfer_amount;
	private String name;
	private String mobile_number;
	private String email_address;
	private String account_number;
	private String ifsc;
	
	@Steps
	moneyTransferSteps steps;
	
	@Title("This test will Validate the account")
	@Test
	public void test01() {
		
		ValidatableResponse resp = steps.validateAccount(reference_id, transfer_amount, name, mobile_number, email_address, account_number, ifsc);
		
		int apiRespCode = resp.extract().statusCode();
		int dbRespCode = databaseValidate.getDctlRespCode(resp.extract().path("decentroTxnId"));
		assertEquals(apiRespCode, dbRespCode);
		
		String respBody = resp.extract().body().asString();
		String schemaText = schemaUtils.getSchemaString("accounts", "validate");
		assertTrue(schemaUtils.validate(schemaText, respBody));
	}
}