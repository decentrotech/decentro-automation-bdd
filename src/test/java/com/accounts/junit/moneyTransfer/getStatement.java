package com.accounts.junit.moneyTransfer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.accounts.cucumber.serenity.moneyTransferSteps;
import com.schemas.schemaUtils;
import com.testbase.moneyTransfer;
import com.utils.databaseValidate;

import io.restassured.response.ValidatableResponse;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import net.thucydides.junit.annotations.UseTestDataFrom;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom("testdata/accounts/moneytransfer/getstatement.csv")
public class getStatement extends moneyTransfer{
	
	private String from;
	private String to;
	private String account_number;
	private String customer_id;
	
	@Steps
	moneyTransferSteps steps;
	
	@Title("This test will fetch the statement for account number")
	@Test
	public void test01() {
		
		ValidatableResponse resp = steps.getStatement(from, to, account_number, customer_id);
		
		int apiRespCode = resp.extract().statusCode();
		int dbRespCode = databaseValidate.getDctlRespCode(resp.extract().path("decentroTxnId"));
		assertEquals(apiRespCode, dbRespCode);
		
		String respBody = resp.extract().body().asString();
		String schemaText = schemaUtils.getSchemaString("accounts", "getStatement");
		assertTrue(schemaUtils.validate(schemaText, respBody));
	}
}