package com.accounts.junit.moneyTransfer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import com.accounts.cucumber.serenity.moneyTransferSteps;
import com.schemas.schemaUtils;
import com.testbase.moneyTransfer;
import com.utils.databaseValidate;
import com.utils.referenceIdGenerator;

import io.restassured.response.ValidatableResponse;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import net.thucydides.junit.annotations.UseTestDataFrom;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom("testdata/accounts/moneytransfer/v2vscenario.csv")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class transactionV2vScenario extends moneyTransfer {
	
	static String reference_id = referenceIdGenerator.getRandomString();
	static String decentro_txn_id = null;
	float preTransBalance1;
	float preTransBalance2;
	float postTransBalance1;
	float postTransBalance2;
	float commissionAmount = 0.0f;
	
	private String from_customer_id;
	private String to_customer_id;
	private String from_account;
	private String to_account;
	private String mobile_number;
	private String transfer_type;
	private String transfer_amount;

	@Steps
	moneyTransferSteps steps;
	
	@Title("Fetch Pre-Transaction Balance for Account 1")
	@Test
	public void test01() {
		ValidatableResponse resp = steps.getBalance(from_account, from_customer_id);
		preTransBalance1 = resp.extract().path("presentBalance");
		
		int apiRespCode = resp.extract().statusCode();
		int dbRespCode = databaseValidate.getDctlRespCode(resp.extract().path("decentroTxnId"));
		assertEquals(apiRespCode, dbRespCode);
		
		String respBody = resp.extract().body().asString();
		String schemaText = schemaUtils.getSchemaString("accounts", "getBalance");
		assertTrue(schemaUtils.validate(schemaText, respBody));
		}
	
	
	@Title("Fetch Pre-Transaction Balance for Account 2")
	@Test
	public void test02() {
		ValidatableResponse resp = steps.getBalance(to_account, to_customer_id);
		preTransBalance2 = resp.extract().path("presentBalance");
		
		int apiRespCode = resp.extract().statusCode();
		int dbRespCode = databaseValidate.getDctlRespCode(resp.extract().path("decentroTxnId"));
		assertEquals(apiRespCode, dbRespCode);
		
		String respBody = resp.extract().body().asString();
		String schemaText = schemaUtils.getSchemaString("accounts", "getBalance");
		assertTrue(schemaUtils.validate(schemaText, respBody));
		}
	
	@Title("Initiate Transaction from Account 1 to Account 2")
	@Test
	public void test03() {
		 	ValidatableResponse resp = steps.initiateTransfer(reference_id, from_customer_id, from_account, to_account, mobile_number, transfer_type, transfer_amount);
		decentro_txn_id = resp.extract().path("decentroTxnId");
		
		int apiRespCode = resp.extract().statusCode();
		int dbRespCode = databaseValidate.getDctlRespCode(resp.extract().path("decentroTxnId"));
		assertEquals(apiRespCode, dbRespCode);
		
		String respBody = resp.extract().body().asString();
		String schemaText = schemaUtils.getSchemaString("accounts", "initiate");
		assertTrue(schemaUtils.validate(schemaText, respBody));
		}
	
	@Title("Get Transaction Status of the Transaction")
	@Test
	public void test04() {
		ValidatableResponse resp = steps.getStatusUsingDecentroTxnId(decentro_txn_id);
		
		int apiRespCode = resp.extract().statusCode();
		int dbRespCode = databaseValidate.getDctlRespCode(resp.extract().path("decentroTxnId"));
		assertEquals(apiRespCode, dbRespCode);
		
		String respBody = resp.extract().body().asString();
		String schemaText = schemaUtils.getSchemaString("accounts", "getStatus");
		assertTrue(schemaUtils.validate(schemaText, respBody));
	}
	
	@Title("Fetch Post-Transaction Balance for Account 1")
	@Test
	public void test05() {
		ValidatableResponse resp = steps.getBalance(from_account, from_customer_id);
		postTransBalance1 = resp.extract().path("presentBalance");
		
		int apiRespCode = resp.extract().statusCode();
		int dbRespCode = databaseValidate.getDctlRespCode(resp.extract().path("decentroTxnId"));
		assertEquals(apiRespCode, dbRespCode);
		
		String respBody = resp.extract().body().asString();
		String schemaText = schemaUtils.getSchemaString("accounts", "getBalance");
		assertTrue(schemaUtils.validate(schemaText, respBody));
		}
	
	
	@Title("Fetch Post-Transaction Balance for Account 2")
	@Test
	public void test06() {
		ValidatableResponse resp = steps.getBalance(to_account, to_customer_id);
		postTransBalance2 = resp.extract().path("presentBalance");
		
		int apiRespCode = resp.extract().statusCode();
		int dbRespCode = databaseValidate.getDctlRespCode(resp.extract().path("decentroTxnId"));
		assertEquals(apiRespCode, dbRespCode);
		
		String respBody = resp.extract().body().asString();
		String schemaText = schemaUtils.getSchemaString("accounts", "getBalance");
		assertTrue(schemaUtils.validate(schemaText, respBody));
	}
	
	@Title("Compare Balance of accounts before and after transaction")
	@Test
	public void test07() {
		
		float transferAmount = Float.parseFloat(transfer_amount);
		
		float expectedBalanceInAccount1 = preTransBalance1 - commissionAmount - transferAmount;
		float expectedBalanceInAccount2 = preTransBalance2 + transferAmount;
		
		Assert.assertEquals(postTransBalance1, expectedBalanceInAccount1, 0.0f);
		Assert.assertEquals(postTransBalance2, expectedBalanceInAccount2, 0.0f);	
	}
}