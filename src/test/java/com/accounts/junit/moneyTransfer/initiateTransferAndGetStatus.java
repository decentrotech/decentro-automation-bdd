package com.accounts.junit.moneyTransfer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import com.accounts.cucumber.serenity.moneyTransferSteps;
import com.schemas.schemaUtils;
import com.testbase.moneyTransfer;
import com.utils.databaseValidate;
import com.utils.referenceIdGenerator;

import io.restassured.response.ValidatableResponse;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import net.thucydides.junit.annotations.UseTestDataFrom;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom("testdata/accounts/moneytransfer/initiatetransfer.csv")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class initiateTransferAndGetStatus extends moneyTransfer {
	
	static String reference_id = referenceIdGenerator.getRandomString();
	static String decentro_txn_id = null;
	
	private String from_customer_id;
	private String from_account;
	private String to_account;
	private String mobile_number;
	private String transfer_type;
	private String transfer_amount;
	
//	COmmented change
//	static String reference_id = referenceIdGenerator.getRandomString();
//	static String from_customer_id = "decentro_acc_0";
//	static String from_account = "462515001000000005";
//	static String to_account = "462515001000000008";
//	static String mobile_number = "9545451849";
//	static String transfer_type = "IMPS";
//	static String transfer_amount = "1";
//	static String decentro_txn_id = null;
	
	@Steps
	moneyTransferSteps steps;
	
	@Title("Initiate Transfer from account")
	@Test
	public void test01() {
		
		ValidatableResponse resp = steps.initiateTransfer(reference_id, from_customer_id, from_account, to_account, mobile_number, transfer_type, transfer_amount);
		decentro_txn_id = resp.extract().path("decentroTxnId");
		
		int apiRespCode = resp.extract().statusCode();
		int dbRespCode = databaseValidate.getDctlRespCode(resp.extract().path("decentroTxnId"));
		assertEquals(apiRespCode, dbRespCode);
		
		String respBody = resp.extract().body().asString();
		String schemaText = schemaUtils.getSchemaString("accounts", "initiateTransfer");
		assertTrue(schemaUtils.validate(schemaText, respBody));
	}
	
	@Title("Verify the status of the transaction using Decentro Transaction ID")
	@Test
	public void test02() {
		
		ValidatableResponse resp = steps.getStatusUsingDecentroTxnId(decentro_txn_id);
		
		int apiRespCode = resp.extract().statusCode();
		int dbRespCode = databaseValidate.getDctlRespCode(resp.extract().path("decentroTxnId"));
		assertEquals(apiRespCode, dbRespCode);
		
		String respBody = resp.extract().body().asString();
		String schemaText = schemaUtils.getSchemaString("accounts", "getStatus");
		assertTrue(schemaUtils.validate(schemaText, respBody));
	}
	
	@Title("Verify the status of the transaction using Reference ID")
	@Test
	public void test03() {
		
		ValidatableResponse resp = steps.getStatusUsingReferenceId(reference_id);
		int apiRespCode = resp.extract().statusCode();
		int dbRespCode = databaseValidate.getDctlRespCode(resp.extract().path("decentroTxnId"));
		assertEquals(apiRespCode, dbRespCode);
		
		String respBody = resp.extract().body().asString();
		String schemaText = schemaUtils.getSchemaString("accounts", "getStatus");
		assertTrue(schemaUtils.validate(schemaText, respBody));
	}
}