package com.accounts.junit.moneyTransfer;

import static org.junit.Assert.assertEquals;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import com.accounts.cucumber.serenity.moneyTransferSteps;
import com.testbase.moneyTransfer;
import com.utils.referenceIdGenerator;

import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import net.thucydides.junit.annotations.UseTestDataFrom;


@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom("testdata/accounts/moneytransfer/validatescenario.csv")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class validateScenario extends moneyTransfer{
	
	float preTransBalance = 0.0f;
	float postTransBalance = 0.0f;
	float commissionAmount = 0.0f;
	
	static String reference_id = referenceIdGenerator.getRandomString();
	
	private String transfer_amount;
	private String name;
	private String mobile_number;
	private String email_address;
	private String account_number;
	private String ifsc;
	private String customer_id;
	
	@Steps
	moneyTransferSteps steps;
	
	@Title("Fetch Pre-Transaction Balance for Account")
	@Test
	public void test01() {
		preTransBalance = steps.getBalance(account_number, customer_id)
				.statusCode(200)
				.extract().path("presentBalance");
	}
	
	@Title("Validate Account")
	@Test
	public void test02() {
		steps.validateAccount(reference_id, transfer_amount, name, mobile_number, email_address, account_number, ifsc)
		.statusCode(200);
	}
	
	@Title("Fetch Post-Transaction Balance for Account ")
	@Test
	public void test03() {
		postTransBalance = steps.getBalance(account_number, customer_id)
				.statusCode(200)
				.extract().path("presentBalance");
	}
	
//	@Title("Compare Balance of account before and after transaction")
//	@Test
	public void test04() {
		
		System.out.println("post"+postTransBalance);
		System.out.println("pre"+preTransBalance);
		System.out.println("com"+commissionAmount);
		float transferAmount = Float.parseFloat(transfer_amount);
		float expectedBalanceInAccount = preTransBalance - commissionAmount - transferAmount;
		System.out.println("ex"+expectedBalanceInAccount);
		System.out.println("ta"+transferAmount);
		assertEquals(expectedBalanceInAccount, postTransBalance, 0.0f);
	}
}
