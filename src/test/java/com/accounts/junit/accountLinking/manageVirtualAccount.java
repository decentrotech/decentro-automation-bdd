package com.accounts.junit.accountLinking;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import com.accounts.cucumber.serenity.accountLinkingSteps;
import com.schemas.schemaUtils;
import com.testbase.accountLinking;
import com.utils.databaseValidate;

import io.restassured.response.ValidatableResponse;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import net.thucydides.junit.annotations.UseTestDataFrom;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom("testdata/accounts/accountlinking/manageaccount.csv")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class manageVirtualAccount extends accountLinking {
	
	private String action;
	private String account_number;
	private String type;
	private String customer_id;
	private String transaction_limit;
	private String minimum_balance;
	
	@Steps
	accountLinkingSteps steps;
	
	@Title("Manage Virtual account")
	@Test
	public void test01() {
		ValidatableResponse resp = steps.manageVirtualAccount(action, account_number, type, customer_id, transaction_limit, minimum_balance);
		
		int apiRespCode = resp.extract().statusCode();
		int dbRespCode = databaseValidate.getDctlRespCode(resp.extract().path("decentroTxnId"));
		assertEquals(apiRespCode, dbRespCode);
		
		String respBody = resp.extract().body().asString();
		String schemaText = schemaUtils.getSchemaString("accounts", "manageVirtualAccount");
		assertTrue(schemaUtils.validate(schemaText, respBody));
	}
}