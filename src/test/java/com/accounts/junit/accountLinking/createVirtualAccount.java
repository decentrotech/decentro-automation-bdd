package com.accounts.junit.accountLinking;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import com.accounts.cucumber.serenity.accountLinkingSteps;
import com.schemas.schemaUtils;
import com.testbase.accountLinking;
import com.utils.databaseValidate;

import io.restassured.response.ValidatableResponse;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import net.thucydides.junit.annotations.UseTestDataFrom;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom("testdata/accounts/accountlinking/createaccount.csv")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class createVirtualAccount extends accountLinking {
	
	private String type;
	private String name;
	private String kyc_verified;
	private String kyc_check_decentro;
	
	@Steps
	accountLinkingSteps steps;
	
	@Title("Create Virtual account")
	@Test
	public void test01() {
		ValidatableResponse resp = steps.createVirtualAccount(type, name, kyc_verified, kyc_check_decentro);
		
		int apiRespCode = resp.extract().statusCode();
		int dbRespCode = databaseValidate.getDctlRespCode(resp.extract().path("decentroTxnId"));
		assertEquals(apiRespCode, dbRespCode);
		
		String respBody = resp.extract().body().asString();
		String schemaText = schemaUtils.getSchemaString("accounts", "createVirtualAccount");
		assertTrue(schemaUtils.validate(schemaText, respBody));
	}
}