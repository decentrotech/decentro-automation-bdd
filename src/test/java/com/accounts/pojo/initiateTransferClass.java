package com.accounts.pojo;

public class initiateTransferClass {
	
	private String reference_id;
	private String purpose_message;
	private String from_customer_id;
	private String from_account;
	private String to_account;
	private String mobile_number;
	private String transfer_type;
	private String transfer_amount;
	private String currency_code;
	
	public String getReference_id() {
		return reference_id;
	}
	public void setReference_id(String reference_id) {
		this.reference_id = reference_id;
	}
	public String getPurpose_message() {
		return purpose_message;
	}
	public void setPurpose_message(String purpose_message) {
		this.purpose_message = purpose_message;
	}
	public String getFrom_customer_id() {
		return from_customer_id;
	}
	public void setFrom_customer_id(String from_customer_id) {
		this.from_customer_id = from_customer_id;
	}
	public String getFrom_account() {
		return from_account;
	}
	public void setFrom_account(String from_account) {
		this.from_account = from_account;
	}
	public String getTo_account() {
		return to_account;
	}
	public void setTo_account(String to_account) {
		this.to_account = to_account;
	}
	public String getMobile_number() {
		return mobile_number;
	}
	public void setMobile_number(String mobile_number) {
		this.mobile_number = mobile_number;
	}
	public String getTransfer_type() {
		return transfer_type;
	}
	public void setTransfer_type(String transfer_type) {
		this.transfer_type = transfer_type;
	}
	public String getTransfer_amount() {
		return transfer_amount;
	}
	public void setTransfer_amount(String transfer_amount) {
		this.transfer_amount = transfer_amount;
	}
	public String getCurrency_code() {
		return currency_code;
	}
	public void setCurrency_code(String currency_code) {
		this.currency_code = currency_code;
	}
}
