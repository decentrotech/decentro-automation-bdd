package com.accounts.pojo;

public class manageVirtualAccountClass {
	
	private String action;
	private String account_number;
	private String type;
	private String customer_id;
	private String mobile_number;
	private String transaction_limit;
	private String minimum_balance;
	private String currency_code;
	private String name;
	
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getAccount_number() {
		return account_number;
	}
	public void setAccount_number(String account_number) {
		this.account_number = account_number;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	public String getMobile_number() {
		return mobile_number;
	}

	public void setMobile_number(String mobile_number) {
		this.mobile_number = mobile_number;
	}
	public String getTransaction_limit() {
		return transaction_limit;
	}
	public void setTransaction_limit(String transaction_limit) {
		this.transaction_limit = transaction_limit;
	}
	public String getMinimum_balance() {
		return minimum_balance;
	}
	public void setMinimum_balance(String minimum_balance) {
		this.minimum_balance = minimum_balance;
	}
	public String getCurrency_code() {
		return currency_code;
	}
	public void setCurrency_code(String currency_code) {
		this.currency_code = currency_code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
