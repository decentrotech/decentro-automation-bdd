package com.accounts.cucumber.serenity;

import org.json.simple.JSONObject;

import com.accounts.pojo.manageVirtualAccountClass;
import com.mysql.cj.xdevapi.JsonString;
import com.utils.getHeadersJson;
import com.utils.referenceIdGenerator;

import io.restassured.response.ValidatableResponse;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class accountLinkingSteps {
	
	JSONObject headers = getHeadersJson.getHeader("Account Headers");
	
	@Step("Manage Virtual Account: {}")
	public ValidatableResponse manageVirtualAccount(String action, String account_number, String type, String customer_id, String transaction_limit, String minimum_balance) {
		
		manageVirtualAccountClass mvaBody = new manageVirtualAccountClass();
		mvaBody.setAccount_number(account_number);
		mvaBody.setAction(action);
		mvaBody.setCustomer_id(customer_id);
		mvaBody.setType(type);
		mvaBody.setMinimum_balance(minimum_balance);
		mvaBody.setTransaction_limit(transaction_limit);
//		mvaBody.setCurrency_code("inr");
//		mvaBody.setName("Ron Weasley");
		
		return SerenityRest.rest().given().relaxedHTTPSValidation()
					.contentType("application/json")
					.headers(headers)
					.log().all()
				.when()
					.body(mvaBody)
					.put("/manage_virtual_account")
				.then()
				.log().all();
	}
	
	@Step("Create Virtual Account: {}")
	public ValidatableResponse createVirtualAccount(String type, String name, String kyc_verified, String kyc_check_decentro) {
		String mobile_number = referenceIdGenerator.getPhoneNumber();
		String customer_id = referenceIdGenerator.getCustomerId();
		JSONObject cvaBody = new JSONObject();
		cvaBody.put("type", type);
		cvaBody.put("name", name);
		cvaBody.put("mobile_number", mobile_number);
		cvaBody.put("kyc_verified", kyc_verified);
		cvaBody.put("kyc_check_decentro", kyc_check_decentro);
		cvaBody.put("customer_id", customer_id);
		cvaBody.put("currency_code", "INR");
		String body = cvaBody.toJSONString();
		
		return SerenityRest.rest().given().relaxedHTTPSValidation()
					.log().all()
					.contentType("application/json")
					.headers(headers)
				.when()
					.body(body)
					.post("/create_virtual_account")
				.then()
					.log().all();
	}
}