package com.accounts.cucumber.serenity;

import org.json.simple.JSONObject;

import com.utils.getHeadersJson;

import io.restassured.response.ValidatableResponse;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class accountInformationSteps {
	
	JSONObject headers = getHeadersJson.getHeader("Account Headers");
	
	@Step("Fetch linked accounts details for customer ID{1}")
	public ValidatableResponse getLinkedAccounts(String mobile, String customer_id) {
		return SerenityRest.rest().given().relaxedHTTPSValidation()
				.headers(headers)
				.queryParam("mobile", mobile)
				.queryParam("customer_id", customer_id)
			.when()
				.get("/fetch_details")
			.then();
	}
	
	
	@Step("Fetch account details for account: {1} ")
	public ValidatableResponse getAccountDetails(String type, String account_number) {
		return SerenityRest.rest().given().relaxedHTTPSValidation()
				.headers(headers)
				.queryParam("type", type)
				.queryParam("account_number", account_number)
			.when()
				.get("/fetch_details")
			.then();
	}
}