package com.accounts.cucumber.serenity;

import org.json.simple.JSONObject;

import com.accounts.pojo.initiateTransferClass;
import com.utils.getHeadersJson;

import io.restassured.response.ValidatableResponse;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class moneyTransferSteps {
	
	static String purpose_message = "This is for test scenario purpose only";
	static String currency_code = "INR";
	JSONObject headers = getHeadersJson.getHeader("Account Headers");
	
	@Step("Initiate {5} Transfer of {6} Rs. from Account: {2} to Account: {3}")
	public ValidatableResponse initiateTransfer(String reference_id, String from_customer_id, String from_account, String to_account, String mobile_number, String transfer_type, String transfer_amount) {
		
		initiateTransferClass initiateTransferBody = new initiateTransferClass();
		initiateTransferBody.setReference_id(reference_id);
		initiateTransferBody.setPurpose_message(purpose_message);
		initiateTransferBody.setFrom_customer_id(from_customer_id);
		initiateTransferBody.setFrom_account(from_account);
		initiateTransferBody.setTo_account(to_account);
		initiateTransferBody.setMobile_number(mobile_number);
		initiateTransferBody.setTransfer_type(transfer_type);
		initiateTransferBody.setTransfer_amount(transfer_amount);		
		initiateTransferBody.setCurrency_code(currency_code);
		
		return	SerenityRest.rest().given().relaxedHTTPSValidation()
					.contentType("application/json")
					.headers(headers)
				.when()
					.body(initiateTransferBody)
					.post("/initiate")
				.then();
	}
	
	@Step("Get Status of transaction with Decentro Transaction ID: {0}")
	public ValidatableResponse getStatusUsingDecentroTxnId(String decentro_txn_id) {
		return SerenityRest.rest().given().relaxedHTTPSValidation()
					.headers(headers)
					.queryParam("decentro_txn_id", decentro_txn_id)
				.when()
					.get("/get_status")
				.then();
	}
	
	@Step("Get Status of transaction with Reference ID: {0}")
	public ValidatableResponse getStatusUsingReferenceId(String reference_id) {
		
		return SerenityRest.rest().given().relaxedHTTPSValidation()
				.headers(headers)
				.queryParam("reference_id", reference_id)
			.when()
				.get("/get_status")
			.then();
	}
	
	@Step("Fetch Balance of account: {0} with customer ID: {1}")
	public ValidatableResponse getBalance(String account_number, String customer_id) {
		
		return SerenityRest.rest().given().relaxedHTTPSValidation()
					.headers(headers)
					.queryParam("account_number", account_number)
					.queryParam("customer_id", customer_id)
				.when()
					.get("/get_balance")
				.then();
	}
	
	@Step("Fetch Statement of account: {2} from {0} to {1}")
	public ValidatableResponse getStatement(String from, String to, String account_number, String customer_id) {
		
		return SerenityRest.rest().given().relaxedHTTPSValidation()
				.headers(headers)
				.queryParam("from", from)
				.queryParam("to", to)
				.queryParam("account_number", account_number)
				.queryParam("customer_id", customer_id)
			.when()
				.get("/get_statement")
			.then();
	}
	
	@Step("Validate Bank Account with Account Number: {5}")
	public ValidatableResponse validateAccount(String reference_id, String transfer_amount, String name, String mobile_number, String email_address, String account_number, String ifsc) {
		JSONObject ben = new JSONObject();
		ben.put("name", name);
		ben.put("mobile_number", mobile_number);
		ben.put("email_address", email_address);
		ben.put("account_number", account_number);
		ben.put("ifsc", ifsc);
		JSONObject body = new JSONObject();
		body.put("beneficiary_details", ben);
		body.put("reference_id", reference_id);
		body.put("transfer_amount", transfer_amount);
		body.put("purpose_message", purpose_message);
		String jsonBody = body.toJSONString();
		
		return	SerenityRest.rest().given().relaxedHTTPSValidation()
				.contentType("application/json")
				.headers(headers)
				.log().all()
			.when()
				.body(jsonBody)
				.post("/validate_account")
			.then()
			.log().all();
	}
}