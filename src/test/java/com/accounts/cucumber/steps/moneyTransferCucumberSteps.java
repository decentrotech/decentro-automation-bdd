package com.accounts.cucumber.steps;

import com.accounts.cucumber.serenity.moneyTransferSteps;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class moneyTransferCucumberSteps {
	
	@Steps
	moneyTransferSteps steps;
	
	@When("^account number and headers are given receive a 200 status code and presentBalance key in the response$")
	public void verifyStatusCode() {
		
		steps.getBalance("FINTECDEC00000000N", "dcntr_cust_001")
		.statusCode(200);
	}

}
