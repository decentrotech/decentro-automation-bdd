package com.utils;

public class headersManager {
	
	public static String getHeaderFileName() {

		String env = envRead.getEnv("environment");
		String company = envRead.getEnv("company");		
		
		String headerFile ="";
	     
		if (env.equals("staging")) {
			switch(company)
			{
			case("Decentro"):
				headerFile = "headers1";	
			}
		}
		else if(env.equals("qa")) {
			switch(company) {
			case("Decentro"):
				headerFile = "headers2";
			case("Gringotts"):
				headerFile = "headers3";
			}
		}
		return headerFile;
	}

}
