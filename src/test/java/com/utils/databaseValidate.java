package com.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

public class databaseValidate {

	
	public static void main(String[] args) {
//		int test = getDctlRespCode("A930398BD6EF441C8F516F3146C987C3");
//		int test = getDctlRespCode("2c8e56d2-f7c9-4612-807d-498e31aca4f3");
		boolean test = cvaCheck("AUTO9014");
		System.out.println(test);
	}
	
	public static boolean cvaCheck(String customer_id) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date now = new Date(); //now
		Date targetTime = DateUtils.addMinutes(now, -60);
		String checkTime = formatter.format(targetTime);
	    System.out.println(checkTime);
	    boolean check1 = false;	//Consumer table null checks
	    boolean check2 = false;	//Consumer bank account null checks
		Connection con = Connector.getConnection();
		String query1 = "SELECT encrypted_name, hashed_name, is_decentro_consumer, date_of_registration, company_consumer_id, consumer_type_id, is_kyc_verified, is_kyc_decentro_verified, created_on, updated_on, company_id, hashed_mobile_number, encrypted_mobile_number FROM consumer WHERE company_consumer_id = ? and created_on > ?";
		String query2 = "SELECT consumer_id, company_consumer_id, provider_id, encrypted_account_name, hashed_account_name, encrypted_account_number, is_deleted, hashed_account_number, ifsc_code, account_type, current_balance, minimum_balance, transaction_limit, created_on, updated_on FROM consumer_bank_account WHERE company_consumer_id = ? and created_on = ?";
		try {
			PreparedStatement st1 = con.prepareStatement(query1);
			st1.setString(1, customer_id);
			st1.setString(2, checkTime);
			
			PreparedStatement st2 = con.prepareStatement(query2);
			st2.setString(1, customer_id);
			st2.setString(2, checkTime);
			
			ResultSet rs1 = st1.executeQuery();
			rs1.next();
			check1 = isMyResultSetEmpty(rs1);
			
			ResultSet rs2 = st2.executeQuery();
			rs2.next();
			check2 = isMyResultSetEmpty(rs2);

			st1.close();
			st2.close();
			con.close();
		}
		catch (SQLException e) {
			System.out.println("Exception in SQL Statements in getDctlRespCode Block");
			e.printStackTrace();
		}
		if (check1 & check2) {
			System.out.println(check1 +"EEE" +check2);
			return true;
		}
		else
			return false;
	}
	
	public static boolean isMyResultSetEmpty(ResultSet rs) throws SQLException {
		return !rs.first();
	}
	
	public static int getDctlRespCode(String txnId) {
		Connection con = Connector.getConnection();
		String query = "SELECT * FROM decentro_company_transaction_log WHERE decentro_company_urn = ?";
		int dbRespCode = 0;
		
		try {
			PreparedStatement st = con.prepareStatement(query);
			st.setString(1, txnId);
			
			ResultSet rs = st.executeQuery();
			rs.next();
		
			dbRespCode = rs.getInt("http_status_code");
			st.close();
			con.close();
		}
		catch (SQLException e) {
			System.out.println("Exception in SQL Statements in getDctlRespCode Block");
			e.printStackTrace();
		}
		return dbRespCode;
	}

//	From Consumer Table
	public static String getHashedEmailAddress(String consumer_id) {
		Connection con = Connector.getConnection();
		String query = "SELECT * FROM consumer WHERE company_consumer_id= ?";
		String dbEmailAddress = null;
		
		try {
			PreparedStatement st = con.prepareStatement(query);
			st.setString(1, consumer_id);
			
			ResultSet rs = st.executeQuery();
			rs.next();
		
			dbEmailAddress = rs.getString("hashed_email_address");
			st.close();
			con.close();
		}
		catch (SQLException e) {
			System.out.println("Exception in SQL Statements in getHashedEmailAddress Block");
			e.printStackTrace();
		}
		return dbEmailAddress;
	}
	
//	From Consumer Table
	public static String getHashedName(String customer_id) {
		Connection con = Connector.getConnection();
		String query = "SELECT * FROM consumer WHERE company_consumer_id= ?";
		String dbName = null;
		
		try {
			PreparedStatement st = con.prepareStatement(query);
			st.setString(1, customer_id);
			
			ResultSet rs = st.executeQuery();
			rs.next();
		
			dbName = rs.getString("hashed_name");
			st.close();
			con.close();
		}
		catch (SQLException e) {
			System.out.println("Exception in SQL Statements in getHashedName Block");
			e.printStackTrace();
		}
		return dbName;
	}
	
//	From Consumer Bank Account Table
	public static String getHashedAccountNumber(String customer_id) {
		Connection con = Connector.getConnection();
		String query = "SELECT * FROM CONSUMER_BANK_ACCOUNT WHERE COMPANY_CONSUMER_ID = ?";
		String dbAccountNumber = null;
		
		try {
			PreparedStatement st = con.prepareStatement(query);
			st.setString(1, customer_id);
			
			ResultSet rs = st.executeQuery();
			rs.next();
		
			dbAccountNumber = rs.getString("hashed_name");
			st.close();
			con.close();
		}
		catch (SQLException e) {
			System.out.println("Exception in SQL Statements in getHashedAccountNumber Block");
			e.printStackTrace();
		}
		return dbAccountNumber;
	}
	
}