package com.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class envRead {
	public static String getEnv(String envKey) {
		Properties p = new Properties();
		InputStream is = null;
		try {
			is = new FileInputStream("env");
		} catch (FileNotFoundException e) {
			System.out.println("Environment file not found");
			e.printStackTrace();
		}
		try {
			p.load(is);
		} catch (IOException e) {
			System.out.println("Error in loading env file");
			e.printStackTrace();
		}
		
		String value = p.getProperty(envKey);
		return value;
	
	}
	
	public static void main(String[] args) throws IOException {
		System.out.println("*********"+getEnv("url")+"*********");
	}

}
