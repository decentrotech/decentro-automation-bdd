package com.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class kycDbValidate {

	
	public static void main(String[] args) {
		int test = getDctlRespCode("A930398BD6EF441C8F516F3146C987C3");
//		int test = getDctlRespCode("2c8e56d2-f7c9-4612-807d-498e31aca4f3");
//		boolean test = prValidateCheck("-123-", "DCTRTX00000100000061MH", "SUCCESS");
//		boolean test = ocrValidateCheck("DCTRTX00000100000061OE", "63e3d199-f2d3-4a13-8168-1bee90077233", "1", "SUCCESS", "SUCCESS");
//		System.out.println(getDuplicateReferenceId());
		System.out.println(test);
	}
	
	public static int getDctlRespCode(String txnID) {
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("JDBC Class not found exception occurred");
			e.printStackTrace();
		}
		
		Connection con = null;
		try {
			con = DriverManager.getConnection(envRead.getEnv("url"), envRead.getEnv("username"), envRead.getEnv("password"));
		} catch (SQLException e) {
			System.out.println("Error in establishing connection to Database");
			e.printStackTrace();
		}
		
//		if (con != null) 
//			System.out.println("Successfully connected to MySQL database");
		
		String query = "SELECT * FROM decentro_company_transaction_log WHERE decentro_company_urn = ?";
		int dbRespCode = 0;
		
		try {
			PreparedStatement st = con.prepareStatement(query);
			st.setString(1, txnID);
			
			ResultSet rs = st.executeQuery();
			rs.next();
		
			dbRespCode = rs.getInt("http_status_code");
			st.close();
			con.close();
		}
		catch (SQLException e) {
			System.out.println("Exception in SQL Statements in getDctlRespCode Block");
			e.printStackTrace();
		}
		return dbRespCode;
	}
	
	public static boolean prValidateCheck(String referenceId, String txnID, String kycStatus) {

		/*
		 * c1 ---> dptl and dctl http_response_code match [query1 and query2]
		 * c2 ---> kyc_history table, status_of_kyc_check match [query3]
		 */ 
//		String dti = "DCTRTX00001200000004R1";
        String query1 = "SELECT * FROM decentro_company_transaction_log WHERE decentro_company_urn = ?";
        String query2 = "SELECT * FROM decentro_provider_transaction_log WHERE decentro_company_transaction_log_id = ?";
        String query3 = "SELECT * FROM kyc_history WHERE company_urn = ?";
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("JDBC Class not found exception occurred");
			e.printStackTrace();
		}
		Connection con = null;
		try {
			con = DriverManager.getConnection(envRead.getEnv("url"), envRead.getEnv("username"), envRead.getEnv("password"));
		} catch (SQLException e) {
			System.out.println("Error in establishing connection to Database");
			e.printStackTrace();
		}
		
//		if (con != null) 
//			System.out.println("Successfully connected to MySQL database");
		
		PreparedStatement st1= null, st2 = null, st3 = null;
		int dptlCode = 0, dctlCode = 0, status_of_kyc_check = 0;
		
		try {
			st1 = con.prepareStatement(query1);
			st1.setString(1, txnID);
			ResultSet rs1 = st1.executeQuery();
			rs1.next();
		
			int dctl_ID = rs1.getInt("id");
			dctlCode = rs1.getInt("http_status_code");
//			System.out.println("DCTL ID for "+txnID +" is: "+dctl_ID);
//			System.out.println("Decentro Status Code: "+dctlCode);
					
//			Query #2
			st2 = con.prepareStatement(query2);
			st2.setLong(1, dctl_ID);
			ResultSet rs2 = st2.executeQuery();
			rs2.next();
			
			dptlCode = rs2.getInt("http_status_code");
//			System.out.println("Status code from Provider: "+dptlCode);
			
//			Query #3
			st3 = con.prepareStatement(query3);
			st3.setString(1, referenceId);
			ResultSet rs3 = st3.executeQuery();
			rs3.next();
			
			status_of_kyc_check = rs3.getInt("status_of_kyc_check");
			
			st1.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Exception in SQL Statements in prValidateCheck Block");
			e.printStackTrace();
		}
		
		boolean c1, c2;
		if (dctlCode == dptlCode)
		{
//			System.out.println("Status Code check passed");
			c1= true;
		}
		else
		{
			System.out.println("Status code from provider doesn't match with status code of Decentro.");
			c1 = false;
		}

		if ((kycStatus.equalsIgnoreCase("Success") & status_of_kyc_check == 1) || (kycStatus.equalsIgnoreCase("Failure") & status_of_kyc_check == 2)
				|| (kycStatus.equalsIgnoreCase("Unknown") & status_of_kyc_check == 3) || (kycStatus.equalsIgnoreCase("Pending") & status_of_kyc_check == 4))
			c2 = true;
		else
			c2 = false;
		
//		System.out.println("c1: "+c1+" c2: "+c2);
		if (c1 & c2)
			return true;
		else
			System.out.println("Status in response body doesn't match Status in database.");
			return false;
	}
	
	public static boolean ocrValidateCheck(String txnID, String referenceId, String kycValidate, String kycStatus, String ocrStatus) {
		/* 
		 * c1 ---> dptl and dctl http_response_code match [query1 and query2]
		 * c2 ---> kyc_history table, status_of_ocr_check match [query3]
		 * c3 ---> kyc_history table, status_of_kyc_check match if kyc_validate is 1 in request [query3]
		 */
		String query1 = "SELECT * FROM decentro_company_transaction_log WHERE decentro_company_urn = ?";
        String query2 = "SELECT * FROM decentro_provider_transaction_log WHERE decentro_company_transaction_log_id = ?";
        String query3 = "SELECT * FROM kyc_history WHERE company_urn = ?";
        
        try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("JDBC Class not found exception occurred");
			e.printStackTrace();
		}
		Connection con = null;
		try {
			con = DriverManager.getConnection(envRead.getEnv("url"), envRead.getEnv("username"), envRead.getEnv("password"));
		} catch (SQLException e) {
			System.out.println("Error in establishing connection to Database");
			e.printStackTrace();
		}
		
//		if (con != null) 
//			System.out.println("Successfully connected to MySQL database");
		
		PreparedStatement st1= null, st2 = null, st3 = null;
		int dptlCode = 0, dctlCode = 0, status_of_ocr_check = 0, status_of_kyc_check = 0;
		
		try {
			st1 = con.prepareStatement(query1);
			st1.setString(1, txnID);
			ResultSet rs1 = st1.executeQuery();
			rs1.next();
		
			int dctl_ID = rs1.getInt("id");
			dctlCode = rs1.getInt("http_status_code");
//			System.out.println("DCTL ID for "+txnID +" is: "+dctl_ID);
//			System.out.println("Decentro Status Code: "+dctlCode);
					
//			Query #2
			st2 = con.prepareStatement(query2);
			st2.setLong(1, dctl_ID);
			ResultSet rs2 = st2.executeQuery();
			rs2.next();
			
			dptlCode = rs2.getInt("http_status_code");
//			System.out.println("Status code from Provider: "+dptlCode);
			
//			Query #3
			st3 = con.prepareStatement(query3);
			st3.setString(1, referenceId);
			ResultSet rs3 = st3.executeQuery();
			rs3.next();
			
			status_of_ocr_check = rs3.getInt("status_of_ocr_check");
			status_of_kyc_check = rs3.getInt("status_of_kyc_check");
			
			st1.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Exception in SQL statements in ocrValidateCheck Block");
			e.printStackTrace();
		}
		
		boolean c1, c2, c3;
		if (dctlCode == dptlCode)
		{
//			System.out.println("Status Code check passed");
			c1= true;
		}
		else
		{
			System.out.println("Status code from provider doesn't match with status code of Decentro.");
			c1 = false;
		}
		
		
		if ((ocrStatus.equalsIgnoreCase("Success") & status_of_ocr_check == 1) || (ocrStatus.equalsIgnoreCase("Failure") & status_of_ocr_check == 0)
				|| (ocrStatus.equalsIgnoreCase("Unknown") & status_of_ocr_check == 3) || (ocrStatus.equalsIgnoreCase("Pending") & status_of_ocr_check == 4))
				c2=true;
		else
			c2 = false;
		
		
		if (kycValidate.equalsIgnoreCase("1")) {
			if ((kycStatus.equalsIgnoreCase("Success") & status_of_kyc_check == 1) || (kycStatus.equalsIgnoreCase("Failure") & status_of_kyc_check == 2)
					|| (kycStatus.equalsIgnoreCase("Unknown") & status_of_kyc_check == 3) || (kycStatus.equalsIgnoreCase("Pending") & status_of_kyc_check == 4))
				c3 = true;
			else
				c3 = false;
		}
		else
			c3 = true;
		
//		System.out.println("c1: "+c1+" c2: "+c2+" c3: "+c3);
		
		if (c1 & c2 & c3)
			return true;
		else
			return false;
	}
	
	public static String getDuplicateReferenceId() {
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("JDBC Class not found exception occurred");
			e.printStackTrace();
		}
		
		Connection con = null;
		try {
			con = DriverManager.getConnection(envRead.getEnv("url"), envRead.getEnv("username"), envRead.getEnv("password"));
		} catch (SQLException e) {
			System.out.println("Error in establishing connection to Database");
			e.printStackTrace();
		}
		
//		if (con != null) 
//			System.out.println("Successfully connected to MySQL database");
		
		String query = "SELECT * FROM decentro_company_transaction_log WHERE id = 10";
		String referenceId = null;
		
		try {
			Statement st = con.createStatement();  
			ResultSet rs = st.executeQuery(query);  
			rs.next(); 
		
			referenceId = rs.getString("company_urn");
			st.close();
			con.close();
		}
		catch (SQLException e) {
			System.out.println("Exception in SQL Statements in getDuplicateReferenceId Block");
			e.printStackTrace();
		}
		return referenceId;
	}
}