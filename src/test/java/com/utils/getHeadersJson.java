package com.utils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class getHeadersJson {
	
	public static JSONObject getHeader(String moduleName) {

		String resourcePath = envRead.getEnv("resource_path");
		
		String headerFile = headersManager.getHeaderFileName();
		
		String path = resourcePath +String.format("testdata/headers/%s.json",headerFile);
		
		JSONParser parser = new JSONParser();
		FileReader reader = null;
		try {
			reader = new FileReader(path);
		} catch (FileNotFoundException e) {
			System.out.println("Header file not found");
			e.printStackTrace();
		}
		Object obj = null;
		try {
			obj = parser.parse(reader);
		} catch (IOException e) {
			System.out.println("Error in reading header file");
			e.printStackTrace();
		} catch (ParseException e) {
			System.out.println("Error in parsing JSON headers");
			e.printStackTrace();
		}
		
		JSONObject jsonObject =  (JSONObject) obj;
		
		JSONObject headers = (JSONObject) jsonObject.get(moduleName);
		
		return headers;
	}

public static void main(String[] args) {
	JSONObject obj = getHeader("Account Headers");
	
	System.out.println("Account Headers: "+obj);
}

}