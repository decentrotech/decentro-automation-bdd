package com.utils;

import java.util.Random;
import java.util.UUID;

public class referenceIdGenerator {
	
	public static String getRandomString() {
		
		String uuid = UUID.randomUUID().toString();
		return uuid;
	}
	
	public static String getPhoneNumber() {
		
        int num = 0;
        int num1 = 0;
        int num2 = 0;
        int num3 = 0;
        
        Random generator = new Random();
        num = generator.nextInt(4)+6;//number should be 6,7,8 or 9
        num1 = generator.nextInt(90) + 10; //number has to be 2 digits
        num2 = generator.nextInt(900) + 100; //number has to be 3 digits
        num3 = generator.nextInt(9000) + 1000; //number has to be 4 digits
        
        String number = Integer.toString(num)+Integer.toString(num1)+Integer.toString(num2)+Integer.toString(num3);
//        System.out.println(number);
		return number;
	}
	
	public static String getCustomerId() {
		Random generator = new Random();
		String num = "AUT"+Integer.toString(generator.nextInt(9999));
		return num;
	}
}