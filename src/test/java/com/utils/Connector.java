package com.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connector {

	private static Connector connector;
	private static Connection connection;
	
	private Connector() {
	}
	
	public synchronized static Connector getInstance() {
	    if (connector == null) {
	        connector = new Connector();
	    }
	    return connector;
	}
	
	public static Connection getConnection() {
	    if (connection == null) {
	        Connection c = null;
	        try {
	            Class.forName("com.mysql.cj.jdbc.Driver");
	        } catch (ClassNotFoundException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
	        try {
	            c = DriverManager.getConnection(envRead.getEnv("url"), envRead.getEnv("username"), envRead.getEnv("password"));
	        } catch (SQLException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
	
	        return c;
	    }
	    return connection;
	    }
}