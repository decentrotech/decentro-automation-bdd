package com.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

public class fetchFile {

	
	public static String imagePathForOcr(String document_type) {
		/*
		 * fetch files one after the other from one folder
		 * each time you run the pgm, fetch a random number and traverse to that folder to get the file
		 */
		int no = fetchNumberOfFolders(document_type);
		Random ran = new Random();
		int n = 1+ran.nextInt(no);

		String resourcePath = envRead.getEnv("resource_path");
		String filePath = resourcePath+"testdata/kyc/testData/"+document_type + "/" + n +"/image.png";
//		System.out.println("Path:       "+filePath);
		return filePath;
	}
	
	
	public static int fetchNumberOfFolders(String type) {
		
		String resourcePath = envRead.getEnv("resource_path");
		File file = new File(resourcePath+"testData/kyc/testData/"+type);
		File[] files = file.listFiles(new FileFilter() {
		    @Override
		    public boolean accept(File f) {
		        return f.isDirectory();
		    }
		});
//		System.out.println("Folders count: " + files.length);
		return files.length;
		
	}
	
	
	public static String getAadhaarXml(int n, String doc) {
		
		String resourcePath=envRead.getEnv("resource_path");
		String fileName="";
		if (doc.equalsIgnoreCase("xml"))
			fileName = "aadhaar_string.txt";
		else if (doc.equalsIgnoreCase("pw"))
			fileName="passcode.txt";	
		String filePath = resourcePath+"testdata/kyc/testData/aadhaar/"+n+"/"+fileName;
		File file = new File(filePath);
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
			e.printStackTrace();
		}
		
		StringBuilder sb = new StringBuilder();
        String line = null;
		try {
			line = br.readLine();
		} catch (IOException e) {
			System.out.println("Error in reading csv file");
			e.printStackTrace();
		}

        while (line != null) {
            sb.append(line);
//            sb.append("\n");
            try {
				line = br.readLine();
			} catch (IOException e) {
				System.out.println("Error in reading csv file");
				e.printStackTrace();
			}
        }
        return sb.toString();
	}
	
	
	public static String errorData() {
		
		String resourcePath=envRead.getEnv("resource_path");
		String filePath = resourcePath+"testdata/kyc/testData/errorTestData/aadhaar_string.txt";
		File file = new File(filePath);
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e1) {
			System.out.println("File not found");
			e1.printStackTrace();
		}
		
		StringBuilder sb = new StringBuilder();
        String line = null;
		try {
			line = br.readLine();
		} catch (IOException e) {
			System.out.println("Error in reading txt file");
			e.printStackTrace();
		}

        while (line != null) {
            sb.append(line);
//            sb.append("\n");
            try {
				line = br.readLine();
			} catch (IOException e) {
				System.out.println("Error in reading txt file");
				e.printStackTrace();
			}
        }
        return sb.toString();
	}
	
	
	public static void main(String[] args) {
//		String path = errorData();
//		System.out.println(path.length());
//		String path = imagePathForOcr("pan");
//		String path = getAadhaarXml(5,"xml");
		System.out.println("test");
		int path = fetchNumberOfFolders("aadhaar");
		System.out.println(path);
	}
}