package com.testbase;

import org.junit.BeforeClass;

import io.restassured.RestAssured;

public class publicRegistry {
	
	@BeforeClass
	public static void init() {
		RestAssured.baseURI = "https://in.qa.decentro.tech/kyc/public_registry";
	}
}
