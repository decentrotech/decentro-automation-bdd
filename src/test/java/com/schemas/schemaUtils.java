package com.schemas;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.utils.ValidationUtils;

public class schemaUtils {
	
	public static String getSchemaString(String module, String api_name) {
		
		InputStream is = null;
		String path = "src/test/java/com/schemas/"+module+"/"+api_name+"-schema.json";
		try {
			is = new FileInputStream(path);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        String schemaText = null;
		try {
			schemaText = IOUtils.toString(is, "UTF-8");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
//		System.out.println("Schema Text: "+schemaText);
		return schemaText;
	}
	
	public static boolean validate(String schemaText, String respBody) {
		
		try {
			if (ValidationUtils.isJsonValid(schemaText, respBody)) {
//				System.out.println("Valid!");
				return true;
			}
			else {
				System.out.println("Schema validation failed!");
				return false;
			}
		} catch (ProcessingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return false;
	}
	
	public static void main(String[] args) {
		JSONObject test = new JSONObject();
		test.put("decentroTxnId", null);
		test.put("responseCode", "S00001");
		test.put("success", "success");
		test.put("message", "Testing purpose only");
		test.put("transferType", "IMPS");
		String resp = test.toString();
		
		String schemaText = getSchemaString("accounts", "initiateTransfer");
		
		validate(schemaText, resp);
		
	}
}